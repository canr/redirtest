from requests.exceptions import ConnectTimeout, ConnectionError, TooManyRedirects
from unittest import mock, TestCase

from murl import Url

from redirtest.tests import RedirectTest


def mocked_requests_head(*args, **kwargs):
    class MockResponse:
        def __init__(self, request_url, response_status_code, response_location_header):
            self.url = request_url
            self.status_code = response_status_code
            self.headers = {
                'location': response_location_header
            }

    url = args[0]
    path = Url(url).path

    if path == '' or path == '/fail':
        return MockResponse(url, 200, '')

    elif path == '/pass':
        response = MockResponse('http://example.com', 200, '')
        response.history = [MockResponse(url, 302, 'http://example.com')]
        return response

    elif path == '/loop':
        raise TooManyRedirects()

    elif path == '/connection_error':
        raise ConnectionError()

    elif path == '/timeout':
        raise ConnectTimeout()

    elif path == '/intermediate1':
        response = MockResponse('http://example.com', 200, '')
        response.history = [
            MockResponse('http://example.com/intermediate1', 302, 'http://example.com/intermediate2'),
            MockResponse('http://example.com/intermediate2', 302, 'http://example.com/')
        ]
        return response

    else:
        return MockResponse(url, 500, '')


class TestRedirectTest(TestCase):
    def test___eq__(self):
        test_1 = RedirectTest('http://example.com', 'http://example.com/')
        test_2 = RedirectTest('http://example.com', 'http://example.com/')
        self.assertEqual(test_1, test_2)

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_run_pass(self, mock_head):
        test = RedirectTest('http://example.com/pass', 'http://example.com')
        test.run()
        self.assertTrue(test.is_pass())

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_run_incorrect(self, mock_head):
        test = RedirectTest('http://example.com/fail', 'http://example.com')
        test.run()
        self.assertFalse(test.is_pass())

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_run_loop(self, mock_head):
        test = RedirectTest('http://example.com/loop', 'http://example.com')
        test.run()
        self.assertFalse(test.is_pass())
        self.assertTrue(test.is_loop)

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_run_connection_error(self, mock_head):
        test = RedirectTest('http://example.com/connection_error', 'http://example.com')
        test.run()
        self.assertFalse(test.is_pass())
        self.assertTrue(test.is_connection_error)

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_run_timeout(self, mock_head):
        test = RedirectTest('http://example.com/timeout', 'http://example.com')
        test.run()
        self.assertFalse(test.is_pass())
        self.assertTrue(test.is_timeout)

    @mock.patch('requests.head', side_effect=mocked_requests_head)
    def test_trace(self, mock_head):
        test = RedirectTest('http://example.com/intermediate1', 'http://example.com/')
        test.run()
        trace = test.trace()
        self.assertEqual(trace,
                         "302 http://example.com/intermediate1\n302 http://example.com/intermediate2\n200 "
                         "http://example.com")
