from unittest import TestCase

from redirtest.cli import get_tests, parse_test_file, run_test
from redirtest.tests import RedirectTest


class TestCli(TestCase):
    def test_get_tests_file(self):
        target = 'tests/fixtures/redirect_tests/valid/google'
        port = None
        tests = get_tests(target, port)
        self.assertEqual(tests, [RedirectTest('http://google.com', 'http://www.google.com')])

    def test_get_tests_directory(self):
        target = 'tests/fixtures/redirect_tests/valid'
        port = None
        tests = get_tests(target, port)
        self.assertEqual(tests, [
            RedirectTest('http://example.com/pass', 'http://example.com'),
            RedirectTest('http://google.com', 'http://www.google.com')
        ])

    def test_get_tests_invalid_file(self):
        target = 'tests/fixtures/redirect_tests/invalid/invalid'
        port = None
        self.assertRaises(SyntaxError, get_tests, target=target, port=port)

    def test_get_tests_nonexistent_file(self):
        target = 'tests/fixtures/redirect_tests/invalid/nonexistent_file'
        port = None
        self.assertRaises(FileNotFoundError, get_tests, target=target, port=port)

    def test_parse_test_file(self):
        target = 'tests/fixtures/redirect_tests/valid/google'
        port = None
        tests = parse_test_file(target, port)
        self.assertEqual(tests, [
            RedirectTest('http://google.com', 'http://www.google.com')
        ])

    def test_parse_test_file_invalid_file(self):
        target = 'tests/fixtures/redirect_tests/invalid/invalid'
        port = None
        self.assertRaises(SyntaxError, parse_test_file, test_file=target, port=port)

    def test_run_test(self):
        test = RedirectTest('http://example.com', 'http://example.com/')
        run_test(test, True, False)
        self.assertIsNotNone(test.response)
