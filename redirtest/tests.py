import requests
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects


class RedirectTest:

    def __init__(self, given_url, expected_url):
        self.given_url, self.expected_url = given_url, expected_url
        self.response = None
        self.is_loop = False
        self.is_connection_error = False
        self.is_timeout = False

    def __eq__(self, other):
        return self.given_url == other.given_url and self.expected_url == other.expected_url

    def run(self):
        try:
            self.response = requests.head(self.given_url, allow_redirects=True, timeout=5)
        except TooManyRedirects as err:
            self.is_loop = True
            self.response = err.response
        except Timeout as err:
            self.is_timeout = True
            self.response = err.response
        except ConnectionError as err:
            self.is_connection_error = True
            self.response = err.response

    def is_pass(self):
        return not self.is_loop and not self.is_connection_error and not self.is_timeout \
                and self.response.url == self.expected_url

    def result(self):
        return "[{}] {} -> {}".format(
            "PASS" if self.is_pass() else "FAIL",
            self.given_url,
            self.expected_url)

    def trace(self):
        return '\n'.join(["{} {}".format(res.status_code, res.url) for res in self.response.history + [self.response]])
