#! /bin/env python3
import argparse
from colorama import init, Fore, Style
from murl import Url
from os import listdir, path
from sys import exit

from redirtest.tests import RedirectTest


def parse_args():
    parser = argparse.ArgumentParser(description='Verifies that given URLs redirect to their targets.')
    parser.add_argument('target', help='The test file or directory of test files to test.')
    parser.add_argument('-p', '--port', type=int,
                        help='Specify the port over which the requests will be sent. Default is 80.')
    parser.add_argument('-q', '--quiet', action='store_true',
                        help='Run the script in quiet mode. Suppresses passing tests.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Run the script in verbose mode. Produces traces for passing tests.')
    return parser.parse_args()


def parse_test_file(test_file, port=None):
    """
    Parses a given test file for tests.
    :param test_file: The path to the file which defines the tests.
    :param port: The number of the port to append to each test in the file.
    :return: List of :func:`~redirtest.tests.RedirectTest` objects defined in the file.
    :raises: SyntaxError: If the given file contains invalid test specifications
    """
    tests = []
    with open(test_file, 'r') as test_file_contents:
        for line in test_file_contents:

            # Skip comments
            if line.strip() == '' or line[0] == '#':
                continue

            # Parse URLs
            urls = [Url(token) for token in line.split()]
            if port:
                for url in urls:
                    url.port = port
            urls = [str(url) for url in urls]

            # Verify number of URLs
            if len(urls) != 2:
                raise SyntaxError("Invalid line: {}".format(line))

            tests.append(RedirectTest(str(urls[0]), str(urls[1])))

        return tests


def run_test(test, quiet, verbose):
    """
    Runs a given test and outputs its results to the command line.
    :param test: The :func:`~redirtest.tests.RedirectTest` to be run.
    :param quiet: Whether the test's results should be ignored unless it fails.
    :param verbose: Whether the test's results should be output with a full trace regardless of whether it passes.
    """
    test.run()
    if test.is_pass():
        if not quiet:
            print(colorize(test.result()))
            if verbose:
                print(test.trace())
            print()
    else:
        print(colorize(test.result()))
        if test.is_loop:
            print("Redirect loop detected. Tracing last 30 hops.")
        elif test.is_timeout:
            print("The connection to {} timed out.".format(test.given_url))
        elif test.is_connection_error:
            print("Unable to connect to {}.".format(test.given_url))

        print(test.trace())
        print()


def get_tests(target, port):
    """
    Gets all tests specified by the target.
    :param target: The target file or directory from which test definitions should be read.
    :param port: The number of the port to append to each test in the file.
    :return: List of :func:`~redirtest.tests.RedirectTest` objects defined in the file or directory.
    :raises: FileNotFoundError: If the given target is neither directory nor file.
    """
    tests = []
    if path.isfile(target):
        tests = parse_test_file(target, port)
    elif path.isdir(target):
        for file in listdir(target):
            for test in parse_test_file(path.join(target, file), port):
                tests.append(test)
    else:
        raise FileNotFoundError("Target {} is not a valid file or directory.".format(target))

    return tests


def colorize(resultstr):
    return resultstr \
        .replace('PASS', Fore.GREEN + 'PASS' + Style.RESET_ALL) \
        .replace('FAIL', Fore.RED + 'FAIL' + Style.RESET_ALL)


def main():
    init()  # Initialize colorama
    args = parse_args()

    # Parse target tests
    tests = get_tests(args.target, args.port)

    # Run tests
    is_failed = False
    for test in tests:
        run_test(test, args.quiet, args.verbose)
        if not is_failed and not test.is_pass():
            is_failed = True

    # Exit
    if is_failed:
        exit(1)
