#! /usr/bin/env python3

from distutils.core import setup
setup(
    name='redirtest',
    version='1.0.4',
    description='redirtest is a Python module and command-line tool to verify that web redirects are correctly '
                'configured.',
    packages=['redirtest'],
    author='Eric Slenk',
    author_email='slenkeri@anr.msu.edu',
    url='https://gitlab.msu.edu/canr/redirtest',
    install_requires=[
        'colorama',
        'murl',
        'requests'
    ],
    scripts=['bin/redirtest'],
    keywords=['redirect', 'test', 'redirtest', ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Software Development',
    ],
)
