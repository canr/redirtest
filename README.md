[![build status](https://gitlab.msu.edu/canr/redirtest/badges/master/build.svg)](https://gitlab.msu.edu/canr/redirtest/commits/master)

redirtest is a python module and command-line tool to verify that web redirects are correctly configured.

# Installation

## Prerequisites
redirtest requires that Python 3 be installed on the host machine. redirtest also depends on the Python modules `colorama`, `murl`, and `requests`.

## Installing the Module
To install this module use Python's pip utility.

```bash
sudo pip3 install git+https://gitlab.msu.edu/canr/redirtest
```

# Usage

## Writing Tests
Test suites are stored as simple text files. Each line of the file represents a single test. Each line in the file should contain an input URL and an expected final URL. redirtest attempts to visit the URL on the left and verifies whether the server ultimately redirects to the URL on the right.

```
http://example.com/target http://example.com/destination
http://example.com/foo/bar http://example.com/baz
http://example.com/target http://example.com/destination
```

## Running Tests
In order to run a single test pass your test definition file's path as an argument.

```bash
redirtest redirect_tests/example
```

To run multiple tests pass the script a directory whose contents should be multiple test definition files.

```bash
redirtest redirect_tests/
```

### Options

<pre>
-h, --help            Shows the help message and exits.

-p PORT, --port PORT  Specify the port over which the requests will be sent. Default is 80.

-q, --quiet           Run the script in quiet mode. Suppresses passing tests.

-v, --verbose         Run the script in verbose mode. Produces traces for passing tests.
</pre>


## Interpreting Test Output
Each individual test in a test file will output its status (either "PASS" or "FAIL"), its input URL and its desired output URL. For example, the following test requested "http://example.com/target", expected to be redirected to "http://example.com/destination", and was successfully redirected to "http://example.com/destination".

<pre>[PASS] http://example.com/target -> http://example.com/destination</pre>

By default tests which fail will output a trace of all HTTP requests and responses in its history. Each line contains a single request's response code (usually 200, 301, 302, or 404) and the URL of that request. For example,

<pre>
[FAIL] http://example.com/foo/bar -> http://example.com/baz
302 http://example.com/foo/bar
301 http://example.com/intermediate
200 http://example.com/fail
</pre>

The failing test's trace is seen in lines 2-4. The trace indicates that the test:
1. Requested "http://example.com/foo/bar" and received a 302 Found response which redirected the test client to "http://example.com/intermediate".
2. Requested "http://example.com/intermediate" and received a 301 Moved Permanently response which redirected the test client to "http://example.com/fail".
3. Requested "http://example.com/fail" and received a 200 Found response.

We can see that the test's request chain ended at "http://example.com/fail" rather than the expected "http://example.com/baz" and the test therefore fails.
